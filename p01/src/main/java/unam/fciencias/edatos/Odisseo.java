package unam.fciencias.edatos;

import java.util.ArrayList;
import java.util.Collection;

import javafx.application.Application;
import javafx.scene.shape.Circle;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.scene.shape.Line;




/**
 * Hello world!
 *
 */
public class Odisseo extends Application {

    @Override
    public void start(Stage stage) {
        Circle punto1 = new Circle(50, 250, 2.5);
        Line segmento1 = new Line(0,300,50,250);
        Line segmento2 = new Line(50,250, 150,100);
        Collection<Node> nodos = new ArrayList<Node>();
        nodos.add(segmento1);
        nodos.add(punto1);
        nodos.add(segmento2);
        Group root = new Group(nodos);
        Scene scene = new Scene(root, 400, 300);

        stage.setTitle("Primer programa en JavaFX");
        stage.setScene(scene);
        stage.show();
    }


    public static void main( String[] args )
    {
        launch();
    }
}
